#!/usr/bin/python
# This is a simple port-forward / proxy, written using only the default python
# library. If you want to make a suggestion or fix something you can contact-me
# at voorloop_at_gmail.com
# Distributed over MIT license
import socket
import select
import time
import sys
import os
import logging
import errno
from optparse import OptionParser, OptionValueError

buffer_size = 4096

def get_command_line_options():
    ''' Sets up optparse and command line options '''
    usage = "Usage: %prog --listenPort 10051 --forwardPort 443 IpAdress"
    parser = OptionParser(usage=usage)

    parser.add_option("-l", "--listenPort", dest="listenPort",
            type="int",
            help="Set the localhost listening port default is 10051.",
            default=10051)

    parser.add_option("-p", "--forwardPort", dest="forwardPort",
            type="int",
            help="Set the destination Port, default is 443",
            default=443)

    parser.add_option("--logpath", dest="logpath",
            help="Set the log path",
            default='/tmp/lbn-proxy')

    parser.add_option("-d", "--debug",
                  action="store_true", dest="debug", default=False,
                  help="Set Log to debug level")

    return parser.parse_args()



class Forward:
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start(self, host, port):
        try:
            self.forward.connect((host, port))
            return self.forward
        except Exception as inst:
            print("[exception] - {0}".format(inst.strerror))
            return False

class TheServer:
    input_list = []
    channel = {}

    def __init__(self, host, port):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((host, port))
        self.server.listen(200)

    def main_loop(self):
        self.input_list.append(self.server)
        while 1:
            ss = select.select
            inputready, outputready, exceptready = ss(self.input_list, [], [])
            for s in inputready:
                if s == self.server:
                    self.on_accept(s)
                    break

                self.data = s.recv(buffer_size)
                if len(self.data) == 0:
                    self.on_close(s)
                    break
                else:
                    self.on_recv(s)

    def on_accept(self, s):
        forward = Forward().start(forward_to[0], forward_to[1])
        clientsock, clientaddr = self.server.accept()
        if forward:
            logging.info("{0} has connected".format(clientaddr))
            self.input_list.append(clientsock)
            self.input_list.append(forward)
            self.channel[clientsock] = forward
            self.channel[forward] = clientsock
        else:
            logging.info("Can't establish a connection with remote server. Closing connection with client side {0}".format(clientaddr))
            clientsock.close()

    def on_close(self, s):
        logging.info("{0} has disconnected".format(s.getpeername()))
        #remove objects from input_list
        self.input_list.remove(s)
        self.input_list.remove(self.channel[s])
        out = self.channel[s]
        # close the connection with client
        self.channel[out].close()
        # close the connection with remote server
        self.channel[s].close()
        # delete both objects from channel dict
        del self.channel[out]
        del self.channel[s]

    def on_recv(self, s):
        data = self.data
        # here we can parse and/or modify the data before send forward
        logging.debug(data)
        self.channel[s].send(data)

#Create LogFile and define Log LEVEL
def init_log(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            print "Error: unable to create the "+path+" folder for logfile"
            print exc
            sys.exit(1)

    #Define log level
    if options.debug:
        logging.basicConfig(filename=options.logpath+'/lbn-unix-proxy.log',level=logging.DEBUG)
    else:
        logging.basicConfig(filename=options.logpath+'/lbn-unix-proxy.log',level=logging.INFO)

if __name__ == '__main__':
    # Get argument flags and command options
    (options, args) = get_command_line_options()

    # Print out usage if no arguments are present
    if len(args) == 0 :
      print('Usage:')
      print("\tPlease specify the destination adress server ip or dns")
      print('More Help:')
      print("\tFor more help use the --help flag")
      sys.exit(1)

    #Set destination server from Args
    dest = args[0]

    init_log(options.logpath)

    forward_to = (dest, options.forwardPort)
    server = TheServer('', options.listenPort)

    try:
        server.main_loop()
    except KeyboardInterrupt:
        logging.info("Ctrl C - Stopping server")
        print("Ctrl C - Stopping server")
        sys.exit(1)
