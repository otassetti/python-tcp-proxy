Python-tcp-proxy
===================

A simple python tcp proxy based on code from : http://voorloopnul.com/blog/a-python-proxy-in-less-than-100-lines-of-code/

Usage: 
-------------

```
python python-tcp.proxy.py -h
Usage: python-tcp.proxy.py --listenPort 10051 --forwardPort 443 IpAdress

Options:
  -h, --help            show this help message and exit
  -l LISTENPORT, --listenPort=LISTENPORT
                        Set the localhost listening port default is 10051.
  -p FORWARDPORT, --forwardPort=FORWARDPORT
                        Set the destination Port, default is 443
  --logpath=LOGPATH     Set the log path
  -d, --debug           Set Log to debug level
```


